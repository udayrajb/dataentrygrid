# dataentrygrid

Data entry grid is a simple user interface for entering tabular data.

**Features**

- Spreadsheet like data entry user interface
- Autocompletion for previous values
- Automatic addition of a new row at the end
